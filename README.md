[![CI Pipeline State](https://gitlab.com/sagidayan/linux-config/badges/main/pipeline.svg)](https://gitlab.com/sagidayan/linux-config/-/commits/main)
# Configurations and Applications
## Using ansible playbooks

Playbooks will install software and configure most of the tools.

Since dotfiles are the most basic thing this repo provides, It is constantly a WIP.

I do try to make sure playbooks work on fedora:latest via gitlab CI.

This repo was created for my own use, But feel free to use it as you please.

Looking just for dotfiles? Take a look at `files/dotfiles`

### Requirements:
 - git
 - Ansible (>=2.9.27)


### Running plays

Playbooks are located in `playbooks/`

```
$ ./run_play.sh <playbook> # Defaults to workstation
```

### Playbooks
 - workstation
 - cli-tools
 - self-managed-unix-update (Updating all packages from the modern unix role)
