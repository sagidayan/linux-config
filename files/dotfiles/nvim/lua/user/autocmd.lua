-- update tree if open on file enter
local api = vim.api
api.nvim_create_autocmd('BufEnter', {
    callback = function()
        local tree_ok, tree_api = pcall(require, "nvim-tree.api")
        if tree_ok and tree_api.tree.is_visible() then
            --           local win_number = api.nvim_get_current_win()
            tree_api.tree.find_file()
        end
    end


})
