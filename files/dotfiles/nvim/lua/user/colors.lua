-- Coloschemes that support TreeSitter:
-- https://github.com/nvim-treesitter/nvim-treesitter/wiki/Colorschemes
local vars = require('user.vars')

local have_theme = false

local gb_status_ok, gruvbox = pcall(require, 'gruvbox')
if gb_status_ok then
    gruvbox.setup({})
    have_theme = true
end

local nightfox_status_ok, nightfox = pcall(require, 'nightfox')
if nightfox_status_ok then
    have_theme = true
    nightfox.setup({
        options = {
            dim_inactive = false
        }
    })
end
vim.opt.background = "dark"
local colorscheme = vars.themes[vars.colorscheme].colorscheme
if have_theme then
    vim.cmd("colorscheme " .. colorscheme)
end


if vars.transparent_bg then
    vim.cmd("highlight Normal guibg=none ctermbg=NONE")
    vim.cmd("highlight NormalNC guibg=none ctermbg=NONE")
    vim.cmd("hi NvimTreeNormal guibg=NONE ctermbg=NONE")
end
