require("user.packer")
vim.cmd(":PackerInstall")


require("user.settings")
require("user.lsp")
require("user.init_plugins")
require("user.cmp")
require("user.treesitter")
require("user.keymaps")
require("user.lualine")
require("user.colors")
require("user.autocmd")

-- Make sure plugins are installed
--vim.cmd(":PackerClean")
