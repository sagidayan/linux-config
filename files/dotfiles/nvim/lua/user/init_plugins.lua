--
-- setup some plugins
--

-- NvimTree empty setup using defaults
local tree_status_ok, tree = pcall(require, "nvim-tree")
if tree_status_ok then
    tree.setup()
end

-- Symbols outline
local symbols_status_ok, symbols = pcall(require, "symbols-outline")
if symbols_status_ok then
    symbols.setup()
end

-- Git signs (Gutter)
local gitsigns_status_ok, gitsigns = pcall(require, "gitsigns")
if gitsigns_status_ok then
    gitsigns.setup {
        current_line_blame = true, -- Toggle with `:Gitsigns toggle_current_line_blame`
        current_line_blame_opts = {
            virt_text = true,
            virt_text_pos = 'eol', -- 'eol' | 'overlay' | 'right_align'
            delay = 1000,
            ignore_whitespace = false,
        },
        current_line_blame_formatter = '[<abbrev_sha>] <author>, <author_time:%Y-%m-%d> - <summary>',
    }
end

-- Telescope
local _, _ = pcall(require, "telescope.builtin")

-- Autopairs
local autopairs_status_ok, autopairs = pcall(require, "nvim-autopairs")
if autopairs_status_ok then
    autopairs.setup {}
end

-- Catppuccin colors
local catppuccin_status_ok, catppuccin = pcall(require, "catppuccin")
if catppuccin_status_ok then
    catppuccin.setup {
    }
end

-- Debugger
local neodev_ok, neodev = pcall(require, "neodev")
local dapui_ok, dapui = pcall(require, "dapui")
local dap_virt_txt_ok, dap_virt_txt = pcall(require, "nvim-dap-virtual-text")
local mason_dap_ok, mason_dap = pcall(require, "mason-nvim-dap")
local dapgo_ok, dapgo = pcall(require, "dap-go")
local dap_python_ok, dap_python = pcall(require, "dap-python")
if dapui_ok then
    dapui.setup()
end
if neodev_ok then
    neodev.setup({
        library = { plugins = { "nvim-dap-ui" }, types = true },
    })
end
if dap_virt_txt_ok then
    dap_virt_txt.setup()
end
if mason_dap_ok then
    mason_dap.setup {
        handlers = {}, -- sets up dap in the predefined manner
    }
end
if dapgo_ok then
    dapgo.setup()
end
if dap_python_ok then
    dap_python.setup("python")
end
-- end debugger
--

-- notifications
local notify_ok, notify = pcall(require, "notify")
if notify_ok then
    vim.notify = notify
end

local harpoon_ok, harpoon = pcall(require, "harpoon")
if harpoon_ok then
    harpoon.setup({
        settings = {
            save_on_toggle = false,
            sync_on_ui_close = true,
        }
    })
end

local codestats_ok, codestats = pcall(require, "codestats")
if codestats_ok then
    local key = os.getenv("CODESTATS_API_KEY") or ""
    if key ~= "" then
        codestats.setup {
          username = 'goomba',  -- needed to fetch profile data
          base_url = 'https://codestats.net',  -- codestats.net base url
          api_key = key,
          send_on_exit = true,  -- send xp on nvim exit
          send_on_timer = true,  -- send xp on timer
          timer_interval = 60000,  -- timer interval in milliseconds (minimum 1000ms to prevent DDoSing codestat.net servers)
          curl_timeout = 5,  -- curl request timeout in seconds
        }
    else
        print("Missing codestats API key... If you want to use CodeStats. Please set CODESTATS_API_KEY env var")
    end
end
