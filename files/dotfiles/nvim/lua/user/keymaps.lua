local Remap = require("user.keymap_func")
local nnoremap = Remap.nnoremap
local vnoremap = Remap.vnoremap
local inoremap = Remap.inoremap
local xnoremap = Remap.xnoremap
local nmap = Remap.nmap



-- NvimTree toggle/refresh/findfile
nnoremap("<C-n>", ":NvimTreeToggle<CR>")
nnoremap("<leader>r", ":NvimTreeRefresh<CR>")
nnoremap("<leader>n", ":NvimTreeFindFile<CR>")


-- debugging
local dapui_ok, dapui = pcall(require, "dapui")
if dapui_ok then
    nnoremap('<leader>dt', dapui.toggle) -- toggles debug view
end
local dap_ok, dap = pcall(require, "dap")
if dap_ok then
    nnoremap('<leader>db', dap.toggle_breakpoint)
    nnoremap('<leader>dc', dap.continue)
end

-- Telescope
local telescope_status_ok, telescope = pcall(require, "telescope.builtin")
if telescope_status_ok then
    nnoremap("<C-p>", telescope.find_files)
    nnoremap("<C-s>", telescope.live_grep)
    nnoremap("<C-f>", telescope.current_buffer_fuzzy_find)
end

-- Harpoon

local harpoon_ok, harpoon = pcall(require, "harpoon")
if harpoon_ok then
    nnoremap("<leader>hx", function() harpoon:list():add() end)
    nnoremap("<leader>hn", function() harpoon:list():next() end)
    nnoremap("<leader>hp", function() harpoon:list():prev() end)
    if telescope_status_ok then
        -- basic telescope configuration
        local conf = require("telescope.config").values
        local function toggle_telescope(harpoon_files)
            local file_paths = {}
            for _, item in ipairs(harpoon_files.items) do
                table.insert(file_paths, item.value)
            end

            require("telescope.pickers").new({}, {
                prompt_title = "Harpoon",
                finder = require("telescope.finders").new_table({
                    results = file_paths,
                }),
                previewer = conf.file_previewer({}),
                sorter = conf.generic_sorter({}),
            }):find()
        end

        nnoremap("<leader>hh", function() toggle_telescope(harpoon:list()) end,
            { desc = "Open harpoon window" })
    else
        nnoremap("<leader>hh", function() harpoon.ui:toggle_quick_menu(harpoon:list()) end)
    end
end

-- Copy to system clipboard
vnoremap('<leader>y', '"+y')
nnoremap('<leader>Y', '"+yg_')
nnoremap('<leader>y', '"+y')
nnoremap('<leader>yy', '"+yy')

-- Paste from system clipboard
nnoremap('<leader>p', '"+p')
nnoremap('<leader>P', '"+P')
vnoremap('<leader>p', '"+p')
vnoremap('<leader>P', '"+P')

-- Easy navigation on split screen
nnoremap('<C-J>', '<C-W><C-J>')
nnoremap('<C-K>', '<C-W><C-K>')
nnoremap('<C-L>', '<C-W><C-L>')
nnoremap('<C-H>', '<C-W><C-H>')

-- Wrap in...
nnoremap('<leader>w"', 'ciw""<Esc>hp')
nnoremap("<leader>w'", "ciw''<Esc>hp")
nnoremap('<leader>w]', 'ciw[]<Esc>hp')
nnoremap('<leader>w[', 'ciw[]<Esc>hp')
nnoremap('<leader>w(', 'ciw()<Esc>hp')
nnoremap('<leader>w)', 'ciw()<Esc>hp')

-- Mardown stuff
-- Toggle checkbox
nnoremap('<leader>x', '^f[ci[x<Esc>')
nnoremap('<leader>X', '^f[ci[<Space><Esc>')
