local servers = {
    "pyright",
    "jsonls",
    "ruff",
}

-- Python virtual environments

local is_vitrual_env = os.getenv("VIRTUAL_ENV") or '' ~= ''
if is_vitrual_env then
    print("Running in python venv")
    vim.g.python3_host_prog = '/venv/bin/python'
    vim.g.python_host_prog = '/venv/bin/python'
end

--

local settings = {
    ui = {
        icons = {
            package_installed = "",
        },
    },
    log_level = vim.log.levels.INFO,
    max_concurrent_installers = 4,
}

require("mason").setup(settings)
require("mason-lspconfig").setup({
    ensure_installed = servers,
    automatic_installation = true,
})

local lspconfig_status_ok, lspconfig = pcall(require, "lspconfig")
if not lspconfig_status_ok then
    return
end

local opts = {}

require('mason-lspconfig').setup_handlers({
    function(server)
        opts = {
            on_attach = require("user.lsp.handlers").on_attach,
            capabilities = require("user.lsp.handlers").capabilities,
        }
        server = vim.split(server, "@")[1]
        -- If we have special options for a given server - merge options with defaults
        local require_ok, conf_opts = pcall(require, "user.lsp.settings." .. server)
        if require_ok then
            opts = vim.tbl_deep_extend("force", conf_opts, opts)
        end

        lspconfig[server].setup(opts)
    end,
})
