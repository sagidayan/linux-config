local vars = require('user.vars')

local status_ok, lualine = pcall(require, 'lualine')
if not status_ok then
    return
end

lualine.setup {
    options = {
        -- theme = vars.colorscheme, --https://github.com/nvim-lualine/lualine.nvim/blob/master/THEMES.md
        theme = vars.themes[vars.colorscheme].lualine_theme,
        component_separators = " ",
        section_separators = { left = "", right = "" },
    }
}
