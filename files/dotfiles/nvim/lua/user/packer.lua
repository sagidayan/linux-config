local is_workstation = os.getenv("DEVICE_ROLE") == 'WORKSTATION'

return require("packer").startup(function(use)
    -- Packer can manage itself
    use 'wbthomason/packer.nvim'

    -- vim-tmux
    use 'christoomey/vim-tmux-navigator'

    -- LSP config
    use {
        'neovim/nvim-lspconfig',
        'williamboman/mason.nvim',
        'williamboman/mason-lspconfig.nvim',
        'hrsh7th/cmp-nvim-lsp',
        'hrsh7th/cmp-buffer',
        'hrsh7th/nvim-cmp',
        'hrsh7th/cmp-nvim-lsp-signature-help',
        'onsails/lspkind.nvim',
        'nvim-lua/lsp_extensions.nvim',
        'glepnir/lspsaga.nvim',
        'simrat39/symbols-outline.nvim',
        'L3MON4D3/LuaSnip',
        'saadparwaiz1/cmp_luasnip',
        'nvimtools/none-ls.nvim',
        'OXY2DEV/markview.nvim',
    }

    -- Debugger
    use { "rcarriga/nvim-dap-ui", requires = { "mfussenegger/nvim-dap", "nvim-neotest/nvim-nio" } }
    use 'theHamsta/nvim-dap-virtual-text'
    use "jay-babu/mason-nvim-dap.nvim"
    use 'leoluz/nvim-dap-go'
    use 'mfussenegger/nvim-dap-python'

    -- file tree
    use {
        'nvim-tree/nvim-web-devicons',
        'kyazdani42/nvim-tree.lua'
    }

    -- Lightline
    --    use 'itchyny/lightline.vim'

    -- Lualine (status line)
    use {
        'nvim-lualine/lualine.nvim',
        requires = { 'kyazdani42/nvim-web-devicons', opt = true }
    }

    -- -- Colorshemes -- --
    -- Gruvbox
    use 'ellisonleao/gruvbox.nvim'
    -- Melange
    use 'savq/melange'
    -- Nightfox
    use 'EdenEast/nightfox.nvim'

    -- Git Gutter
    use {
        'lewis6991/gitsigns.nvim',
        -- tag = 'release'
    }

    -- Notifications
    use 'rcarriga/nvim-notify'

    -- Auto Pairs (brackets)
    use {
        "windwp/nvim-autopairs",
        config = function() require("nvim-autopairs").setup {} end
    }

    -- Telescope (fzf) and Harpoon
    use {
        'nvim-telescope/telescope.nvim',
        requires = { { 'nvim-lua/plenary.nvim' } }
    }
    use {
        "ThePrimeagen/harpoon",
        branch = "harpoon2",
        requires = { { "nvim-lua/plenary.nvim" } }
    }

    if is_workstation then
        use {
            'nvim-treesitter/nvim-treesitter',
            run = ':TSUpdate'
        }
        use 'nvim-treesitter/nvim-treesitter-context'
    end

    -- Beacon
    use 'danilamihailov/beacon.nvim'

    -- Catppuccin
    use { "catppuccin/nvim", as = "catppuccin" }

    -- Code Stats
    use {
        'liljaylj/codestats.nvim', 
        requires = { { "nvim-lua/plenary.nvim" } }
    }
end)
