local M = {}

M.colorscheme = 'catppuccin'
M.transparent_bg = false

M.themes = {
    gruvbox = {
        colorscheme = 'gruvbox',
        lualine_theme = 'gruvbox',
    },
    melange = {
        colorscheme = 'melange',
        lualine_theme = 'OceanicNext',
    },
    nightfox = {
        colorscheme = 'nightfox',
        lualine_theme = 'nightfox'
    },
    catppuccin = {
        colorscheme = 'catppuccin-mocha',
        lualine_theme = 'catppuccin'
    }
}

-- Gruvbox
local st_ok, palette = pcall(require, M.colorscheme .. '.palette')
if st_ok then
    M.themes.gruvbox.highlight_bg_color = palette.gray
end
-- melange
st_ok, palette = pcall(require, M.colorscheme .. '.palettes.dark')
if st_ok then
    M.themes.melange.highlight_bg_color = palette.a.sel
end


return M
