#!/bin/bash
########################################################
# DO NOT EDIT - WILL BE DISCARDED IN NEXT PLAYBOOK RUN #
########################################################

# Get docs fast
SELECTED=`cat ~/.shellconfig/cheat/topics | fzf --header="🏳️ Select a Topic: " --reverse`
if [[ -z $SELECTED ]]; then
    exit 0
fi

read -p "🏳️ What do you want to know about ${SELECTED}?: " query

query=`echo $query | tr ' ' '+'`
if [[ $SELECTED == "TLDR" ]]; then
    tmux neww -n "[🏳️ Cheat: ${SELECTED}/${query}]" bash -c "tldr ${query} | bat --paging=always --file-name='${SELECTED} | Query: ${query}'"
else
    tmux neww -n "[🏳️ Cheat: ${SELECTED}/${query}]" bash -c "curl -s https://cht.sh/$SELECTED/$query | bat --paging=always --file-name='Topic: ${SELECTED} | Query: ${query}'"
fi
