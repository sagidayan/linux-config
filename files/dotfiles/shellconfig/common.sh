#!/bin/bash
########################################################
# DO NOT EDIT - WILL BE DISCARDED IN NEXT PLAYBOOK RUN #
########################################################


############################################
#         Shell aliases
############################################
alias :q=exit
alias :Q=exit

alias ls='lsd'
alias f='printf "\033c"'
alias top=btm
alias vim=nvim
alias v=nvim

alias t=tmux


############################################
#         Environment Variables
############################################

# bat theme (New cat) $ bat --list-themes
gruvbox="gruvbox"
monokai="Monokai Extended"
nord="Nord"

export BAT_THEME="$nord"


export VISUAL=nvim;
export EDITOR=nvim;



############################################
#         Shell functions
############################################

function gitB() {
    if [ -d ./.git ]; then
        CURRENT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
        if [ $(git branch | wc -l) -lt 2 ]; then 
            echo "⚠️  Only ${CURRENT_BRANCH} exists."
            return 1
        fi
        BRANCH=$(git branch -l |sed '/\*/d'| fzf --reverse --header="Current Branch: ${CURRENT_BRANCH}. Select Branch to checkout")
        if [ -z ${BRANCH} ]; then
            echo ❌  Canceled
        else   
            BRANCH=$(echo $BRANCH | xargs)
            echo "Swiching to ${BRANCH}"
            git ck "${BRANCH}"
        fi
    else
        echo "❌ Not in a git repository"
    fi;
    echo "✅  Switched to ${BRANCH} branch successfully"
}


function huh() {
    CURRENT_DIR=$(echo "${PWD##*/}")
    TITLE="${1:=$CURRENT_DIR}"

    echo "=-=-=-=-=-=-=-=-=-=-=-=-=-="; \
    echo "${TITLE}"; \
    echo "=-=-=-=-=-=-=-=-=-=-=-=-=-="; \
    echo;echo Status:; \
    echo -=-=-=-; \
    git status; \
    echo; \
    echo Remotes:; \
    echo -=-=-=-=; \
    git remote -v | cat; \
    echo;echo;

}

function pre_check_tmux_for_layout() {
    ERROR_PREFIX="[ERROR]: "

    if [[ -z $(which tmux) ]]; then
        echo ""
        echo "$ERROR_PREFIX Missing dependencie of tmux";
        echo ""
        return 1
    fi

    if [[ -z ${TMUX-} ]];then
        echo ""
        echo "$ERROR_PREFIX Must run in a tmux session";
        echo ""
        return 1
    fi

    if [ $(tmux list-panes | wc -l) -ne 1 ]; then
        echo ""
        echo "$ERROR_PREFIX Please run '$1' in a clean tmux window (No panes)"
        echo ""
        return 1
    fi
}

# Opens a dedicated note file for current directory
function open_project_notes() {
    CURRENT_DIR=$(echo "${PWD##*/}")
    NOTES_DIR="${HOME}/Documents/notes"
    # make sure notes folder exists
    mkdir -p ${NOTES_DIR}
    FILE="${NOTES_DIR}/${CURRENT_DIR}.md"
    if [ ! -f "$FILE" ]; then
        cat <<EOF >> ${FILE}
${CURRENT_DIR} Notes
---

#### Today:
- [ ] Add Notes

#### Tomorrow:

#### Yesterday:

EOF
    fi

    vim "${FILE}"

}
# Creates a tmux layout of 3 columns with mid col has a vertical split for use as well
# Usage:    $ ide 
#           $ ide name_for_workspace
function ide() {
    CURRENT_DIR=$(echo "${PWD##*/}")
    IDE_NAME="${1:=$CURRENT_DIR}"

    pre_check_tmux_for_layout "ide" || return 1
    clear; 
    tmux split-window -h -l 70
    tmux split-window -v
    tmux send -t"$session_uuid:.0" "vim ." Enter
    tmux send -t"$session_uuid:.1" "open_project_notes" Enter
    tmux send -t"$session_uuid:.2" "huh '${IDE_NAME}'" Enter
    tmux rename-window ${IDE_NAME}
    tmux select-pane -t 0
}

# Create a tmux layout with one command
# Usage:    $ layout "<num_rows> [<num_rows>]+ [layout_name]"
# Examples: $ layout "2" Bob # Single column with 2 rows, named Bob
#           $ layout "2 1" # Two columns, left column with 2 rows, right column with ine row
function layout() {
    pre_check_tmux_for_layout "layout/grid" || return 1
    CURRENT_DIR=$(echo "${PWD##*/}")
    LAYOUT_NAME="${2:=$CURRENT_DIR}"
    INPUT=${1-}
    if [ -z $INPUT ]; then
        echo "Error: Invalid layout. Usage: $ layout <layout>"
        return 1
    fi

    COLS=($(echo $INPUT))
    # echo "Number of Columns: ${#COLS[@]}"

    # Create columns
    H_RATIO=$((100 / ${#COLS[@]}))
    cols=$((${#COLS[@]} - 1))
    if [ $cols -le 0  ]; then
        cols=1
    else
        for col in {1..$(($cols))}
        do
            H_SIZE=$((100 - (col * H_RATIO)))
            tmux split-window -h -l $((H_SIZE))
        done
    fi

    tmux select-pane -t 0

    # Create Rows
    for col in {1..$(($cols + 1))}
    do
        if [ $col != 1 ]; then
            tmux select-pane -R #Move pane to the right
        fi
        ROWS=$((COLS[col]))
        if [ $((ROWS)) -gt 1 ]; then
            V_RATIO=$((100 / (ROWS)))
            for row in {1..$((ROWS - 1))}
            do
                V_SIZE=$((100 - (row * V_RATIO)))
                tmux split-window -v -l $((V_SIZE))
            done
        fi
    done

    tmux select-pane -t 0
    tmux rename-window ${LAYOUT_NAME}
    tmux send-keys "clear" C-m
}

# Create a tmux grid layout. default 2x2
# Usage:    $ grid
#           $ grid 3x3
function grid() {
    #WIP
    pre_check_tmux_for_layout "grid" || return 1
    INPUT=${1:=0x0} 
    GRID_COLS=$(echo $INPUT | grep -oP '^[\d]+')
    GRID_ROWS=$(echo $INPUT | grep -oP '[\d]+$')
    echo "COLS: $GRID_COLS"
    echo "ROWS: $GRID_ROWS"
    if [[ $GRID_ROWS == "0" ]] || [[ $GRID_COLS == "0" ]]; then
        echo "Defaulting to 2x2 grid"
        GRID_COLS="2"
        GRID_ROWS="2"
    fi


    LAYOUT_SCHEME=""
    for i in {1..$((GRID_COLS))}
    do
        LAYOUT_SCHEME="${LAYOUT_SCHEME} ${GRID_ROWS}"
    done

    layout $LAYOUT_SCHEME $2 || return 1
    return 0
}

