#!/bin/bash

JSON_PATH=~/.shellconfig/workspaces/workspaces.json

function yes_no_question() {
    QUESTION=$1
    RESPONSE=$(cat <<EOF | fzf --reverse --header "${QUESTION}"
⨯ No
🗸 Yes
EOF
)
    echo $(echo $RESPONSE | awk '{print $2}')
}

NAME=${1:-}

# check if folder already saved as a workspace
EXISTING_WSP=$(cat $JSON_PATH | jq -r --arg pwd $(pwd) 'to_entries[] | select(.value.dir == $pwd)')
if [ ! -z "$EXISTING_WSP"  ]; then
    WSP_NAME=$(echo "$EXISTING_WSP" | jq -r --arg pwd $(pwd) '.key')
    RESPONSE=$(yes_no_question "⚠️ There is already a workspace named '${WSP_NAME}' for this folder. Would you like to override?")
    if [[ "$RESPONSE" != "Yes"  ]]; then
        echo Canceled
        exit
    fi
    NAME=$WSP_NAME
fi

if [ -z "$NAME" ]; then
    read -p "ℹ️  Please give this workspace a name: " NAME
fi

if [ -z "$NAME" ]; then
    echo "Error: Invalid workspace name"
    exit 1
fi

# Check if workspace already exists
if [ -z "$EXISTING_WSP" ] && [ $(cat $JSON_PATH | jq 'keys' | grep "${NAME}") ]; then
    OVERRIDE=$(yes_no_question "a Worspace with name '${NAME}' already exists. Override?")
    if [[ "$OVERRIDE" == "No" ]]; then
        echo Canceled
        exit
    fi
fi

CURRENT_DIR=$(pwd)

LAYOUT=$(cat <<EOF | fzf --reverse --header "ℹ️  Select a layout/custom-command for ${NAME}" 
ide
grid
layout "2 1"
layout "1 2"
Custom command
EOF
)

# If custom command...
IS_CMD=0
if [[ "${LAYOUT}" == "Custom command" ]]; then
    IS_CMD=1
    read -p "ℹ️  Please provide a command: " LAYOUT
fi

if [ -z "$LAYOUT" ]; then
    echo "Invalid layout/command"
    exit 1
fi

echo $(cat ${JSON_PATH} \
    | jq --arg NAME "$NAME" --arg LAYOUT "$LAYOUT" --arg CURRENT_DIR "$CURRENT_DIR" --arg IS_CMD "$IS_CMD" '. + {($NAME):{layout: $LAYOUT, dir: $CURRENT_DIR, is_cmd: $IS_CMD}}') > ${JSON_PATH}
echo "✅ Added ${NAME}"

