#!/bin/bash

JSON_PATH=~/.shellconfig/workspaces/workspaces.json

function yes_no_question() {
    QUESTION=$1
    RESPONSE=$(cat <<EOF | fzf --reverse --header "${QUESTION}"
⨯ No
🗸 Yes
EOF
)
    echo $(echo $RESPONSE | awk '{print $2}')
}

WSP=$(cat ${JSON_PATH} \
    | jq  -r 'keys[]' \
    | fzf --reverse --header "Select a Workspace to DELETE"
)

if [ -z "$WSP" ]; then
    echo "Aborted"
    exit 1
fi

RESPONSE="$(yes_no_question "❓ Are you sure you want to delete ${WSP}?")"

if [ "$RESPONSE" == "No" ]; then
    echo "Aborted"
    exit
fi

echo $(cat ${JSON_PATH} | jq --arg WSP "$WSP" 'del(.[$WSP])') > ${JSON_PATH}

echo "⚠️  '${WSP}' Was removed from your workspaces"
