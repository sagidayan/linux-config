#!/bin/bash

TARGET_PLAY="${1:-workstation}"
PLAYBOOK="playbooks/${TARGET_PLAY}.yml"

if [ ! -f $PLAYBOOK ]; then
    echo "Invalid play '${TARGET_PLAY}'. Unable to locate ${PLAYBOOK}"
    exit 1
fi

function print_fail_and_exit() {
 echo "❌ Something went wrong..."
 cp /tmp/artifacts/${TARGET_PLAY}*.xml ./report.xml
 exit 1
}

echo "======> Running ${TARGET_PLAY}"

./install-ansible-modules.sh

# Run Playbook
ANSIBLE_CONFIG=$(pwd)/ansible.cfg JUNIT_OUTPUT_DIR="/tmp/artifacts" ansible-playbook ${PLAYBOOK} || print_fail_and_exit

echo "======> Done."
